import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmsComponent } from 'src/app/wiki/containers/films/films.component';
import { PeopleContainerComponent } from 'src/app/wiki/containers/people/people-container.component';
import { PlanetsComponent } from './wiki/containers/planets/planets.component';
import { SpeciesComponent } from './wiki/containers/species/species.component';
import { StarshipsConatinerComponent } from './wiki/containers/starships/starships-conatiner.component';
import { VehiclesComponent } from './wiki/containers/vehicles/vehicles.component';

const routes: Routes = [
  { path: '', redirectTo: '/films', pathMatch: 'full' },
  { path: 'films', component: FilmsComponent },
  { path: 'people', component: PeopleContainerComponent },
  { path: 'species', component: SpeciesComponent },
  { path: 'starships', component: StarshipsConatinerComponent },
  { path: 'vehicles', component: VehiclesComponent },
  { path: 'planets', component: PlanetsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
