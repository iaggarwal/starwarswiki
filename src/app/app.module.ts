import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './/app-routing.module';
import { AppComponent } from './app.component';
import { WikiModule } from './wiki/wiki.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CommonModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot({}),
    AppRoutingModule,
    WikiModule,
    StoreDevtoolsModule.instrument(),
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
