import { EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Resources } from 'src/app/wiki/types/resources';
import { Results } from 'src/app/wiki/types/resources-state';
import { Columns } from './films/types/columns';

export abstract class ResourceBaseComponent implements OnChanges {
  @Input() protected loading: boolean;
  @Input() protected error: boolean;
  @Input() protected count: number;
  @Input() protected next: string;
  @Input() protected previous: string;
  @Input() protected result: Results;
  @Input() protected columnList: Array<Columns>;
  @Input() protected title: string;

  @Output() protected rowClick = new EventEmitter<Resources>();
  @Output() protected pageClick = new EventEmitter<string>();

  protected data: Array<Resources>;

  protected cellClasses: { [key: string]: string } = {};
  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.result && changes.result.currentValue) {
      this.data = Object.values(changes.result.currentValue);
    }

    if (changes.columnList && changes.columnList.currentValue && changes.columnList.currentValue.length > 0) {
      changes.columnList.currentValue.forEach((col: Columns) => {
        this.cellClasses[col.key] = `column-${col.key.replace(/[^a-z0-9_-]/gi, '-')}`;
      });
    }
  }
}
