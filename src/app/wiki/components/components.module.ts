import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilmsComponent } from './films/films.component';

@NgModule({
  imports: [CommonModule],
  declarations: [FilmsComponent],
  exports: [FilmsComponent],
  entryComponents: [FilmsComponent]
})
export class ComponentsModule {}
