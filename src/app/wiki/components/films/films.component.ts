import { Component, Renderer2 } from '@angular/core';
import { ResourceBaseComponent } from '../resource-base.component';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent extends ResourceBaseComponent {
  constructor(private renderer: Renderer2) {
    super();
  }

  calculateClasses(key: string) {
    return {
      [this.cellClasses[key]]: true
    };
  }
}
