import { OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromResources from 'src/app/wiki/store/actions/resources.actions';
import * as fromResourcesReducer from 'src/app/wiki/store/reducers';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { Results } from 'src/app/wiki/types/resources-state';
import { ResourceTypes } from '../types/resources';

export const TEMPLATE_STRING = `<app-films [loading]="loading$ | async"
[error]="error$ | async"
[count]="count$ | async"
[next]="next$ | async"
[previous]="previous$ | async"
[result]="result$ | async"
[title]="title"
[columnList]="columns"
(pageClick)="getResource($event)">
</app-films>`;

export abstract class ResourceBaseContainerComponent implements OnInit {
  protected loading$: Observable<boolean>;
  protected error$: Observable<boolean>;
  protected count$: Observable<number>;
  protected next$: Observable<string>;
  protected previous$: Observable<string>;
  protected result$: Observable<Results>;

  constructor(public resourceType: ResourceTypes, public store$: Store<State>, protected title: string) {
    this.loading$ = this.store$.select(fromResourcesReducer.getResourceLoading(this.resourceType));
    this.error$ = this.store$.select(fromResourcesReducer.getResourceError(this.resourceType));
    this.count$ = this.store$.select(fromResourcesReducer.getResourceCount(this.resourceType));
    this.next$ = this.store$.select(fromResourcesReducer.getResourceNext(this.resourceType));
    this.previous$ = this.store$.select(fromResourcesReducer.getResourcePrevious(this.resourceType));
    this.result$ = this.store$.select(fromResourcesReducer.getResourceResults(this.resourceType));
  }

  ngOnInit() {
    this.store$.dispatch(new fromResources.LoadResources({ resourceType: this.resourceType }));
  }

  getResource(pageUrl: string) {
    this.store$.dispatch(new fromResources.LoadResources({ resourceType: this.resourceType, url: pageUrl }));
  }
}
