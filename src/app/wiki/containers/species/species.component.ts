import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Columns } from 'src/app/wiki/components/films/types/columns';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from 'src/app/wiki/types/resources';

@Component({
  template: TEMPLATE_STRING,
  styleUrls: ['./species.component.scss']
})
export class SpeciesComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns: Array<Columns> = [
    { key: 'name', label: 'Name' },
    { key: 'classification', label: 'Classification' },
    { key: 'designation', label: 'Designation' },
    { key: 'average_height', label: 'Average Height' },
    { key: 'average_lifespan', label: 'Average Lifespan' },
    { key: 'eye_colors', label: 'Eye Colors' },
    { key: 'hair_colors', label: 'Hair Colors' },
    { key: 'skin_colors', label: 'Skin Colors' },
    { key: 'language', label: 'Language' }
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.SPECIES, store$, 'Species');
  }
}
