import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from '../../components/components.module';
import { SpeciesComponent } from './species.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [SpeciesComponent]
})
export class SpeciesModule { }
