import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from '../../components/components.module';
import { PlanetsComponent } from './planets.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [PlanetsComponent]
})
export class PlanetsModule { }
