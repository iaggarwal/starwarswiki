import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Columns } from 'src/app/wiki/components/films/types/columns';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from 'src/app/wiki/types/resources';

@Component({
  template: TEMPLATE_STRING,
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns: Array<Columns> = [
    { key: 'name', label: 'Name' },
    { key: 'diameter', label: 'Diameter' },
    { key: 'rotation_period', label: 'Rotation Period' },
    { key: 'orbital_period', label: 'Orbital Period' },
    { key: 'gravity', label: 'Gravity' },
    { key: 'population', label: 'Population' },
    { key: 'climate', label: 'Climate' },
    { key: 'terrain', label: 'Terrain' },
    { key: 'surface_water', label: 'Surface Water' }
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.PLANETS, store$, 'Planet');
  }
}
