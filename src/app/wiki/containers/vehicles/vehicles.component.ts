import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from 'src/app/wiki/types/resources';

@Component({
  template: TEMPLATE_STRING
})
export class VehiclesComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns = [
    {key: 'name', label: 'Name'},
    {key: 'model', label: 'Model'},
    {key: 'vehicle_class', label: 'Vehicle Class'},
    {key: 'manufacturer', label: 'Manufacturer'},
    {key: 'cost_in_credits', label: 'Cost In Credits'},
    {key: 'length', label: 'Length'},
    {key: 'crew', label: 'Crew'},
    {key: 'passengers', label: 'Passengers'},
    {key: 'max_atmosphering_speed', label: 'Max Atmosphering Speed'}
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.VEHICLES, store$, 'Vehicles');
  }
}
