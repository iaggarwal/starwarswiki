import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Columns } from 'src/app/wiki/components/films/types/columns';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from '../../types/resources';

@Component({
  template: TEMPLATE_STRING,
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns: Array<Columns> = [
    {key: 'title', label: 'Title'},
    {key: 'opening_crawl', label: 'Opening Crawl'},
    {key: 'release_date', label: 'Release Date'}
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.FILMS, store$, 'Films');
  }
}
