import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from 'src/app/wiki/components/components.module';
import { FilmsComponent } from './films.component';

@NgModule({
  imports: [CommonModule, ComponentsModule],
  declarations: [FilmsComponent],
  exports: [FilmsComponent],
  entryComponents: [FilmsComponent]
})
export class FilmsModule {}
