import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Columns } from 'src/app/wiki/components/films/types/columns';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from '../../types/resources';

@Component({
  template: TEMPLATE_STRING,
  styleUrls: ['./people-container.component.scss']
})
export class PeopleContainerComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns: Array<Columns> = [
    { key: 'name', label: 'Name' },
    { key: 'birth_year', label: 'Birth Year' },
    { key: 'eye_color', label: 'Eye Color' },
    { key: 'gender', label: 'Gender' },
    { key: 'hair_color', label: 'Hair Color' },
    { key: 'height', label: 'Height' }
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.PEOPLE, store$, 'People');
  }
}
