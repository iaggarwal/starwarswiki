import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from 'src/app/wiki/components/components.module';
import { PeopleContainerComponent } from './people-container.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [PeopleContainerComponent],
  exports: [PeopleContainerComponent],
  entryComponents: [PeopleContainerComponent]
})
export class PeopleContainerModule {}
