import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from '../../components/components.module';
import { StarshipsConatinerComponent } from './starships-conatiner.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [StarshipsConatinerComponent]
})
export class StarshipsConatinerModule { }
