import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Columns } from 'src/app/wiki/components/films/types/columns';
import { ResourceBaseContainerComponent, TEMPLATE_STRING } from 'src/app/wiki/containers/resource-base-container.component';
import { State } from 'src/app/wiki/store/reducers/resources.reducer';
import { ResourceTypes } from '../../types/resources';

@Component({
  template: TEMPLATE_STRING,
  styleUrls: ['./starships-conatiner.component.scss']
})
export class StarshipsConatinerComponent extends ResourceBaseContainerComponent implements OnInit {
  readonly columns: Array<Columns> = [
    {key: 'name', label: 'Name'},
    {key: 'model', label: 'Model'},
    {key: 'starship_class', label: 'Starship Class'},
    {key: 'manufacturer', label: 'Manufacturer'},
    {key: 'cost_in_credits', label: 'Cost In Credits'},
    {key: 'length', label: 'Length'},
    {key: 'crew', label: 'Crew'},
    {key: 'passengers', label: 'Passengers'},
    {key: 'max_atmosphering_speed', label: 'Max Amosphering Speed'}
  ];
  constructor(public store$: Store<State>) {
    super(ResourceTypes.STARSHIPS, store$, 'Starship');
  }
}
