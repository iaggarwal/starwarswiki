import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipsConatinerComponent } from './starships-conatiner.component';

describe('StarshipsConatinerComponent', () => {
  let component: StarshipsConatinerComponent;
  let fixture: ComponentFixture<StarshipsConatinerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarshipsConatinerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarshipsConatinerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
