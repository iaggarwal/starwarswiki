import { Resources } from 'src/app/wiki/types/resources';

export interface BaseState {
  loading: boolean;
  error: boolean;
  count: number;
  next: string;
  previous: string;
}

export interface ResourceState extends BaseState {
  results: Results;
}


export interface Results {
  [id: string]: Resources;
}
