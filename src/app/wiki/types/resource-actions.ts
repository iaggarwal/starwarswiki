import { ResourceTypes, Resources } from './resources';

export interface BaseResourceAction {
  resourceType: ResourceTypes;
}

export interface LoadResourceAction extends BaseResourceAction {
  url?: string;
}

export interface LoadResourceSuccessAction extends BaseResourceAction {
  count?: number;
  next?: string;
  previous?: string;
  results: {
    [id: string]: Resources;
  };
}
