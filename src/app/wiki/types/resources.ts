import { IFilm } from 'src/app/wiki/containers/films/types/films';
import { IPeople } from 'src/app/wiki/containers/people/types/people';
import { IPlanets } from 'src/app/wiki/containers/planets/types/planets';
import { IStarship } from 'src/app/wiki/containers/starships/types/starships';
import { ISpecies } from '../containers/species/types/species';
import { IVehicles } from '../containers/vehicles/types/vehicles';

export type Resources = IFilm | IPeople | IStarship | IPlanets | IVehicles | ISpecies;

export enum ResourceTypes {
  PEOPLE = 'people',
  FILMS = 'films',
  STARSHIPS = 'starships',
  VEHICLES = 'vehicles',
  SPECIES = 'species',
  PLANETS = 'planets'
}
