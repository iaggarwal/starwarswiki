import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { IGetAllResourceResponse } from 'src/app/wiki/types/base-resource-response';
import { LoadResourceSuccessAction } from 'src/app/wiki/types/resource-actions';
import { Resources } from 'src/app/wiki/types/resources';
import { WikiApiService } from '../../services/wiki-api.service';
import { LoadResources, LoadResourcesFailure, LoadResourcesSuccess, ResourcesActionTypes } from '../actions/resources.actions';

@Injectable()
export class ResourcesEffects {
  constructor(private actions$: Actions, private wikiApiService: WikiApiService) {}

  @Effect()
  load$ = this.actions$.pipe(
    ofType(ResourcesActionTypes.LoadResources),
    switchMap((action: LoadResources) => {
      const RESOURCE_TYPE = action.payload.resourceType;
      const URL = action.payload.url;
      return this.wikiApiService.getResource(RESOURCE_TYPE, URL).pipe(
        map((response: IGetAllResourceResponse<Resources>) => {
          const SUCCESS_PAYLOAD: LoadResourceSuccessAction = {
            ...response,
            resourceType: RESOURCE_TYPE,
            results: response.results.reduce<{ [id: string]: Resources }>(
              (accumulator: { [id: string]: Resources }, currentValue: Resources) => {
                accumulator[currentValue.url] = currentValue;
                return accumulator;
              },
              <{ [id: string]: Resources }>{}
            )
          };

          return new LoadResourcesSuccess(SUCCESS_PAYLOAD);
        }),
        catchError(() => {
          return of(new LoadResourcesFailure({ resourceType: RESOURCE_TYPE }));
        })
      );
    })
  );
}
