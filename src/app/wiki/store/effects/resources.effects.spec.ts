import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable } from 'rxjs';
import { WikiApiService } from '../../services/wiki-api.service';
import { ResourceTypes } from '../../types/resources';
import { LoadResources } from '../actions/resources.actions';
import { ResourcesEffects } from './resources.effects';

describe('ResourcesService', () => {
  let actions$: Observable<any>;
  let effects: ResourcesEffects;
  let wikiService: jasmine.SpyObj<WikiApiService>;
  const responseData = {
    count: 87,
    next: 'https://swapi.co/api/people/?page=2',
    previous: null,
    results: [
      {
        name: 'Luke Skywalker',
        height: '172',
        mass: '77',
        hair_color: 'blond',
        skin_color: 'fair',
        eye_color: 'blue',
        birth_year: '19BBY',
        gender: 'male',
        homeworld: 'https://swapi.co/api/planets/1/',
        films: [
          'https://swapi.co/api/films/2/',
          'https://swapi.co/api/films/6/',
          'https://swapi.co/api/films/3/',
          'https://swapi.co/api/films/1/',
          'https://swapi.co/api/films/7/'
        ],
        species: ['https://swapi.co/api/species/1/'],
        vehicles: ['https://swapi.co/api/vehicles/14/', 'https://swapi.co/api/vehicles/30/'],
        starships: ['https://swapi.co/api/starships/12/', 'https://swapi.co/api/starships/22/'],
        created: '2014-12-09T13:50:51.644000Z',
        edited: '2014-12-20T21:17:56.891000Z',
        url: 'https://swapi.co/api/people/1/'
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ResourcesEffects,
        provideMockActions(() => actions$),
        {
          provide: WikiApiService,
          useValue: jasmine.createSpyObj('WikiApiService', ['getResource'])
        }
      ]
    });

    effects = TestBed.get(ResourcesEffects);
    wikiService = TestBed.get(WikiApiService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  xdescribe('ResourcesEffects', () => {
    let getResourceResponse: Object;
    beforeEach(() => {
      getResourceResponse = cold('-a|', { a: responseData });
      wikiService.getResource.and.returnValue(getResourceResponse);
      actions$ = hot('-a-', {
        a: new LoadResources({
          resourceType: ResourceTypes.PEOPLE
        })
      });
    });

    it('should call getResource function from wiki service', (done: Function) => {
      effects.load$.subscribe(() => {
        expect(wikiService.getResource).toHaveBeenCalled();
      });
    });
  });
});
