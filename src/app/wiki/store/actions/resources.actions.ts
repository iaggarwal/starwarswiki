import { Action } from '@ngrx/store';
import { BaseResourceAction, LoadResourceAction, LoadResourceSuccessAction } from 'src/app/wiki/types/resource-actions';

export enum ResourcesActionTypes {
  LoadResources = '[Resources] LoadResources',
  LoadResourcesSuccess = '[Resources] LoadResourcesSuccess',
  LoadResourcesFailure = '[Resources] LoadResourcesFailure'
}

export class LoadResources implements Action {
  readonly type = ResourcesActionTypes.LoadResources;
  constructor(public payload: LoadResourceAction) {}
}

export class LoadResourcesSuccess implements Action {
  readonly type = ResourcesActionTypes.LoadResourcesSuccess;
  constructor(public payload: LoadResourceSuccessAction) {}
}

export class LoadResourcesFailure implements Action {
  readonly type = ResourcesActionTypes.LoadResourcesFailure;

  constructor(public payload: BaseResourceAction) {}
}

export type ResourcesActions = LoadResources | LoadResourcesSuccess | LoadResourcesFailure;
