import { ResourceTypes } from 'src/app/wiki/types/resources';
import { ResourceState } from 'src/app/wiki/types/resources-state';
import { ResourcesActionTypes, ResourcesActions } from '../actions/resources.actions';

export interface State {
  [ResourceTypes.FILMS]: ResourceState;
  [ResourceTypes.PEOPLE]: ResourceState;
  [ResourceTypes.PLANETS]: ResourceState;
  [ResourceTypes.SPECIES]: ResourceState;
  [ResourceTypes.STARSHIPS]: ResourceState;
  [ResourceTypes.VEHICLES]: ResourceState;
}

export const initialState: State = {
  [ResourceTypes.FILMS]: <ResourceState>{},
  [ResourceTypes.PEOPLE]: <ResourceState>{},
  [ResourceTypes.PLANETS]: <ResourceState>{},
  [ResourceTypes.SPECIES]: <ResourceState>{},
  [ResourceTypes.STARSHIPS]: <ResourceState>{},
  [ResourceTypes.VEHICLES]: <ResourceState>{}
};

export function reducer(state = initialState, action: ResourcesActions): State {
  switch (action.type) {
    case ResourcesActionTypes.LoadResources: {
      const RESOURCE_TYPE = action['payload'].resourceType;
      return {
        ...state,
        [RESOURCE_TYPE]: {
          ...state[RESOURCE_TYPE],
          loading: true
        }
      };
    }
    case ResourcesActionTypes.LoadResourcesSuccess: {
      const RESOURCE_TYPE = action['payload'].resourceType;
      return {
        ...state,
        [RESOURCE_TYPE]: {
          ...state[RESOURCE_TYPE],
          count: action['payload'].count,
          next: action['payload'].next,
          previous: action['payload'].previous,
          loading: false,
          results: action['payload'].count
            ? action['payload'].results
            : {
                ...state[RESOURCE_TYPE].results,
                ...action['payload'].results
              },
          error: false
        }
      };
    }
    case ResourcesActionTypes.LoadResources: {
      const RESOURCE_TYPE = action['payload'].resourceType;
      return {
        ...state,
        [RESOURCE_TYPE]: {
          ...state[RESOURCE_TYPE],
          loading: false,
          error: true
        }
      };
    }
    default:
      return state;
  }
}

// selectors
export const getResource = (resourceType: ResourceTypes) => (state: State) => state[resourceType];
export const getResourceLoading = (state: ResourceState) => state.loading;
export const getResourceError = (state: ResourceState) => state.error;
export const getResourceCount = (state: ResourceState) => state.count;
export const getResourceNext = (state: ResourceState) => state.next;
export const getResourcePrevious = (state: ResourceState) => state.previous;
export const getResourceResults = (state: ResourceState) => state.results;
