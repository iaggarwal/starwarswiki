import { ResourceTypes } from '../../types/resources';
import { LoadResources, LoadResourcesSuccess } from '../actions/resources.actions';
import { State, initialState, reducer } from './resources.reducer';

fdescribe('Resources Reducer', () => {
  let stateFixture: any;
  const responseData = {
    count: 87,
    next: 'https://swapi.co/api/people/?page=2',
    previous: null,
    results: {
      'https://swapi.co/api/people/1/': {
        name: 'Luke Skywalker',
        height: '172',
        mass: '77',
        hair_color: 'blond',
        skin_color: 'fair',
        eye_color: 'blue',
        birth_year: '19BBY',
        gender: 'male',
        homeworld: 'https://swapi.co/api/planets/1/',
        films: [
          'https://swapi.co/api/films/2/',
          'https://swapi.co/api/films/6/',
          'https://swapi.co/api/films/3/',
          'https://swapi.co/api/films/1/',
          'https://swapi.co/api/films/7/'
        ],
        species: ['https://swapi.co/api/species/1/'],
        vehicles: ['https://swapi.co/api/vehicles/14/', 'https://swapi.co/api/vehicles/30/'],
        starships: ['https://swapi.co/api/starships/12/', 'https://swapi.co/api/starships/22/'],
        created: '2014-12-09T13:50:51.644000Z',
        edited: '2014-12-20T21:17:56.891000Z',
        url: 'https://swapi.co/api/people/1/'
      }
    }
  };

  beforeEach(() => {
    stateFixture = Object.freeze(<State>{
      ...initialState
    });
  });
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('LOAD action', () => {
    it('Should return loading true', () => {
      const loadAction: LoadResources = new LoadResources({ resourceType: ResourceTypes.PEOPLE });
      expect(reducer(stateFixture, loadAction)).toEqual({
        ...stateFixture,
        [ResourceTypes.PEOPLE]: {
          ...stateFixture[ResourceTypes.PEOPLE],
          loading: true
        }
      });
    });
  });

  describe('LOAD Success action', () => {
    it('Should return response with loading false', () => {
      const loadAction: LoadResourcesSuccess = new LoadResourcesSuccess({
        resourceType: ResourceTypes.PEOPLE,
        ...responseData
      });
      expect(reducer(stateFixture, loadAction)).toEqual({
        ...stateFixture,
        [ResourceTypes.PEOPLE]: {
          ...stateFixture[ResourceTypes.PEOPLE],
          ...responseData,
          loading: false,
          error: false
        }
      });
    });
  });
});
