import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ResourceTypes } from '../../types/resources';
import * as fromResources from './resources.reducer';

const getResourceState = createFeatureSelector<fromResources.State>('resources');

export const getResource = (resourceType: ResourceTypes) => {
  return createSelector(getResourceState, fromResources.getResource(resourceType));
};
export const getResourceLoading = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourceLoading);
};
export const getResourceError = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourceError);
};
export const getResourceCount = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourceCount);
};
export const getResourceNext = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourceNext);
};
export const getResourcePrevious = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourcePrevious);
};
export const getResourceResults = (resourceType: ResourceTypes) => {
  return createSelector(getResource(resourceType), fromResources.getResourceResults);
};
