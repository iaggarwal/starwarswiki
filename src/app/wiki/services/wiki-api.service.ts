import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IGetAllResourceResponse } from 'src/app/wiki/types/base-resource-response';
import { ResourceTypes, Resources } from 'src/app/wiki/types/resources';

@Injectable()
export class WikiApiService {
  protected readonly swapiBaseUrl = 'https://swapi.co/api/';

  constructor(private httpClient: HttpClient) {}

  getResource(resourceType: ResourceTypes, url?: string): Observable<IGetAllResourceResponse<Resources>> {
    const GET_URL = url || this.swapiBaseUrl + resourceType;
    return this.httpClient.get<IGetAllResourceResponse<Resources>>(GET_URL);
  }
}
