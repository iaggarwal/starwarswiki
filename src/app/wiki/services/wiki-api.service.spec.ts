import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { IGetAllResourceResponse } from 'src/app/wiki/types/base-resource-response';
import { ResourceTypes, Resources } from 'src/app/wiki/types/resources';
import { WikiApiService } from './wiki-api.service';

fdescribe('WikiApiService', () => {
  let wikiApiService: WikiApiService;
  let httpMock: HttpTestingController;
  let responseData: IGetAllResourceResponse<Resources>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WikiApiService]
    });
  });

  beforeEach(() => {
    wikiApiService = TestBed.get(WikiApiService);
    httpMock = TestBed.get(HttpTestingController);
    spyOn(wikiApiService, 'getResource').and.callThrough();
  });

  describe('getResource', () => {
    beforeEach(() => {
      responseData = {
        count: 87,
        next: 'https://swapi.co/api/people/?page=2',
        previous: null,
        results: [
          {
            name: 'Luke Skywalker',
            height: '172',
            mass: '77',
            hair_color: 'blond',
            skin_color: 'fair',
            eye_color: 'blue',
            birth_year: '19BBY',
            gender: 'male',
            homeworld: 'https://swapi.co/api/planets/1/',
            films: [
              'https://swapi.co/api/films/2/',
              'https://swapi.co/api/films/6/',
              'https://swapi.co/api/films/3/',
              'https://swapi.co/api/films/1/',
              'https://swapi.co/api/films/7/'
            ],
            species: ['https://swapi.co/api/species/1/'],
            vehicles: ['https://swapi.co/api/vehicles/14/', 'https://swapi.co/api/vehicles/30/'],
            starships: ['https://swapi.co/api/starships/12/', 'https://swapi.co/api/starships/22/'],
            created: '2014-12-09T13:50:51.644000Z',
            edited: '2014-12-20T21:17:56.891000Z',
            url: 'https://swapi.co/api/people/1/'
          }
        ]
      };
    });

    describe('Without URL', () => {
      it('should call endpoint for people', (done: Function) => {
        wikiApiService.getResource(ResourceTypes.PEOPLE).subscribe(response => {
          expect(response).toEqual(responseData);
          done();
        });
        const req = httpMock.expectOne('https://swapi.co/api/people');
        expect(req.request.method).toEqual('GET');
        req.flush(responseData);
        httpMock.verify();
      });
    });

    describe('With URL', () => {
      const URL = 'https://swapi.co/api/people/page?1';
      it('should call endpoint for people', (done: Function) => {
        wikiApiService.getResource(ResourceTypes.PEOPLE, URL).subscribe(response => {
          expect(response).toEqual(responseData);
          done();
        });
        const req = httpMock.expectOne(URL);
        expect(req.request.method).toEqual('GET');
        req.flush(responseData);
        httpMock.verify();
      });
    });
  });
});
