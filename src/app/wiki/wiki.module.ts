import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ComponentsModule } from './components/components.module';
import { FilmsModule } from './containers/films/films.module';
import { PeopleContainerModule } from './containers/people/people-container.module';
import { PlanetsModule } from './containers/planets/planets.module';
import { SpeciesModule } from './containers/species/species.module';
import { StarshipsConatinerModule } from './containers/starships/starships-conatiner.module';
import { VehiclesModule } from './containers/vehicles/vehicles.module';
import { WikiApiService } from './services/wiki-api.service';
import { ResourcesEffects } from './store/effects/resources.effects';
import * as fromResources from './store/reducers/resources.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('resources', fromResources.reducer),
    EffectsModule.forFeature([ResourcesEffects])
  ],
  exports: [
    FilmsModule,
    PeopleContainerModule,
    ComponentsModule,
    StarshipsConatinerModule,
    PlanetsModule,
    VehiclesModule,
    SpeciesModule
  ],
  providers: [WikiApiService]
})
export class WikiModule {}
