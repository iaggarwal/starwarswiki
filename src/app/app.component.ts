import { Component } from '@angular/core';
import { ResourceTypes } from './wiki/types/resources';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Star wars wiki';
  resources = Object.values(ResourceTypes).sort().map((resource: string) => {
    return { label: resource.charAt(0).toUpperCase() + resource.slice(1), link: resource };
  });
}
